<?php

/**
 * Helper to implementation of hook_context_default_contexts().
 */
function _churches_news_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'churches_news';
  $context->description = 'Context for the main news listing.';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'churches_news' => 'churches_news',
        'churches_news:page_1' => 'churches_news:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-churches_news-block_1' => array(
          'module' => 'views',
          'delta' => 'churches_news-block_1',
          'region' => 'left',
          'weight' => 0,
        ),
      ),
    ),
  );

  $export['churches_news'] = $context;
  return $export;
}

/**
 * Helper to implementation of hook_ctools_plugin_api().
 */
function _churches_news_ctools_plugin_api() {
  $args = func_get_args();
  $module = array_shift($args);
  $api = array_shift($args);
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  else if ($module == "system" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Helper to implementation of hook_strongarm().
 */
function _churches_news_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_churches_news';
  $strongarm->value = '0';

  $export['comment_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_churches_news';
  $strongarm->value = '2';

  $export['comment_default_mode_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_churches_news';
  $strongarm->value = '2';

  $export['comment_default_order_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_churches_news';
  $strongarm->value = '1';

  $export['comment_form_location_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_churches_news';
  $strongarm->value = '0';

  $export['comment_preview_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_churches_news';
  $strongarm->value = '0';

  $export['comment_subject_field_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_churches_news';
  $strongarm->value = 1;

  $export['enable_revisions_page_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_churches_news';
  $strongarm->value = array(
    '0' => 'status',
    '1' => 'promote',
    '2' => 'revision',
  );

  $export['node_options_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_churches_news_pattern';
  $strongarm->value = 'news/[yyyy]/[mm]/[title-raw]';

  $export['pathauto_node_churches_news_pattern'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_churches_news';
  $strongarm->value = 0;

  $export['show_diff_inline_churches_news'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_churches_news';
  $strongarm->value = 0;

  $export['show_preview_changes_churches_news'] = $strongarm;
  return $export;
}
