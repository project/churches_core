<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function churches_news_context_default_contexts() {
  module_load_include('inc', 'churches_news', 'churches_news.defaults');
  $args = func_get_args();
  return call_user_func_array('_churches_news_context_default_contexts', $args);
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function churches_news_ctools_plugin_api() {
  module_load_include('inc', 'churches_news', 'churches_news.defaults');
  $args = func_get_args();
  return call_user_func_array('_churches_news_ctools_plugin_api', $args);
}

/**
 * Implementation of hook_node_info().
 */
function churches_news_node_info() {
  module_load_include('inc', 'churches_news', 'churches_news.features.node');
  $args = func_get_args();
  return call_user_func_array('_churches_news_node_info', $args);
}

/**
 * Implementation of hook_strongarm().
 */
function churches_news_strongarm() {
  module_load_include('inc', 'churches_news', 'churches_news.defaults');
  $args = func_get_args();
  return call_user_func_array('_churches_news_strongarm', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function churches_news_views_default_views() {
  module_load_include('inc', 'churches_news', 'churches_news.features.views');
  $args = func_get_args();
  return call_user_func_array('_churches_news_views_default_views', $args);
}
